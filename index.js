// alert('Hello World')

/*
	While loop

		syntax:
			while(expression/condition) {
				statement
			}
*/

let count = 5

while (count !== 0) {
	console.log('While '+count)
	count-- 
	/*count-- / count -= 1 / count = count-1 */
	/*count -= 5 - pag may specific number ng decrement*/
}

let x = 1
while (x <= 5) {
	console.log(x)
	x++
}

/*
	DO WHILE LOOP
		a do-while loop works a lot like the while loop. But unlike while loops, do-while loops guarantee that the code will be executed at least once.

	syntax:
		do {
			statement
		} while (expression/condition)
*/

/*let number = Number(prompt('Give me a number: '))

do {
	console.log('Do While: '+number)
	number += 1
} while(number < 10)*/


/*
	FOR LOOP
		more flexible than the while loop and the do-while loop.

	syntax:
		for (initialization; expression/condition; finalExpresion) {
			statement
		}
*/

for (let count = 0; count <= 20; count++) {
	console.log('For loop: ' + count)
}

let myString = 'Enteng Kabisote'
console.log(myString.length)

console.log(myString[0])
// index number = first letter is equal to zero
console.log(myString[1])
console.log(myString[14])

for (let x=0; x<myString.length; x++) {
	console.log(myString[x])
}

let myName = 'Francis';
for (let i = 0; i<myName.length; i++) {
	if (
		myName[i].toLowerCase() == 'a' ||
		myName[i].toLowerCase() == 'e' ||
		myName[i].toLowerCase() == 'i' ||
		myName[i].toLowerCase() == 'o' ||
		myName[i].toLowerCase() == 'u' 
		) {
		console.log('Vowels')
	} else {
		console.log(myName[i])
	}
}

// CONTINUE AND BREAK STATEMENTS
/*
	"Continue" statement allows the code to got to the next iteration of the lop without finishing the execution of all statements in a code block

	"Break" statement is used to terminate the current loop once a match has been found
*/ 

for (let count = 0; count <= 20; count++){

	if (count % 2 ===0) {
		continue;
	}
	console.log('Continue and Break: '+ count)

	if (count>10) {
		break;
	}
}

let name = 'Alexandro'
for (let i=0; i<name.length; i++) {

	console.log(name[i]);
	
	if (name[i].toLowerCase() === 'a') {
		console.log('Continue to the next iteration')
		continue;
	}

	if (name[i].toLowerCase() === 'd') {
		break;
	}
}